"""Test to compare backend search results with rendered search results and to check add to basket functionality

Tests are performed for two browsers (Firefox, Chrome) using pytest and selenium libraries.
Corresponding webdrivers for browsers are located in the same library as test file.
test_owasp_search performs reading backend json search results with requests module and compares it with results
acquired using selenium.
test_basket adds two most expensive search results to the basket and checks if they were added properly
You can run tests by running command 'pytest -rs -v'

Run tests on Windows OS
"""

import requests
import pytest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException, WebDriverException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from time import sleep

EXPLICIT_TIMEOUT = 10
N_TRIES = 10

email = 'restream21@mailinator.com'
password = 'qwerty'

# Tested browsers with webdriver reference
browsers = {
    'chrome': webdriver.Chrome,
    'firefox': webdriver.Firefox
}


# Session fixture to get and destroy browser instances
@pytest.fixture(scope='session', params=browsers.keys())
def web_driver(request):

    try:
        _browser = browsers[request.param]()
    except WebDriverException:
        pytest.skip('No appropriate webdriver for {} or {} is not installed'.format(request.param, request.param))
        return

    yield _browser
    _browser.quit()


# Fixture to prepare browser for test (login -> test -> logout)
@pytest.fixture
def browser(web_driver):

    _browser = web_driver
    _browser.implicitly_wait(time_to_wait=2)
    _browser.get('https://restream.sloppy.zone')
    _browser.find_element_by_xpath('//*[@href="#/login"]').click()
    _browser.find_element_by_id('userEmail').send_keys(email)
    _browser.find_element_by_id('userPassword').send_keys(password)
    _browser.find_element_by_id('loginButton').click()

    # Check if we are logged in
    try:
        _browser.find_element_by_xpath('//*[contains(text(), "Invalid email or password.")]')
        pytest.skip('Cannot login with provided credentials')
        return
    except NoSuchElementException:
        pass

    yield _browser

    # Logout
    logout_button = _browser.find_element_by_xpath('//*[@href="#/logout"]')
    logout_button.click()
    sleep(1)            # TODO Change to webdriver explicit wait


# Get backend search results. Search on the rendered page
def search_shop(browser, search_query):

    # Get login cookies from webdriver instance
    token = browser.get_cookie('token')['value']

    # Get json search results from backend
    r = requests.get('https://restream.sloppy.zone/rest/product/search',
                     params={'q': '{}'.format(search_query)}, cookies={'token': token})
    data_backend = r.json()['data']

    # Add Details and Bucket buttons code to results for comparing to rendered results later
    buttons = ('showDetail(product.id)', 'addToBasket(product.id)')

    # Create dict of backend response for each entry in search results
    data_backend = [
        {
            'image': 'https://restream.sloppy.zone/public/images/products/' + x['image'],
            'name': x['name'],
            'description': x['description'],
            'price': str(x['price']),
            'button_detail': buttons[0],
            'button_basket': buttons[1]
        } for x in data_backend
    ]

    # Perform search in browser as well
    search_field = browser.find_element_by_xpath('//input[@placeholder="Search..."]')
    search_field.clear()
    search_field.send_keys('{}'.format(search_query))
    search_field.send_keys(Keys.RETURN)

    return data_backend


# Test to get backend and frontend search results and compare them
def test_owasp_search(browser):

    data_backend = search_shop(browser, 'owasp')
    sleep(1)            # TODO Change to webdriver explicit wait

    # Find every item on search results page and create dict for each item
    data_rendered = []
    WebDriverWait(browser, EXPLICIT_TIMEOUT).until(EC.presence_of_element_located((By.TAG_NAME, 'table')))
    table = browser.find_element_by_tag_name('table')

    WebDriverWait(table, EXPLICIT_TIMEOUT).until(EC.presence_of_element_located((By.TAG_NAME, 'tr')))
    for tr in table.find_elements_by_xpath('.//tr'):
        WebDriverWait(table, EXPLICIT_TIMEOUT).until(EC.presence_of_element_located((By.TAG_NAME, 'td')))
        tds = tr.find_elements_by_xpath('.//td')
        if tds:
            button_detail, button_basket = [a.get_attribute('ng-click') for a in tds[4].find_elements_by_tag_name('a')]
            td_data_rendered = {
                'image': tds[0].find_element_by_xpath('.//img').get_attribute('src'),
                'name': tds[1].get_attribute('innerHTML'),
                'description': tds[2].find_element_by_tag_name('div').get_attribute('innerHTML'),
                'price': tds[3].get_attribute('innerHTML'),
                'button_detail': button_detail,
                'button_basket': button_basket
            }
            data_rendered.append(td_data_rendered)

    # Check if backend and rendered results are the same
    assert data_backend == data_rendered


# Test to place items in basket and check it
def test_basket(browser):

    # Remove everything from the basket
    browser.find_element_by_xpath('//*[@href="#/basket"]').click()
    for i in range(N_TRIES):
        try:
            browser.find_element_by_xpath('//a[@ng-click="delete(product.basketItem.id)"]').click()
            sleep(1)            # TODO Change to webdriver explicit wait
        except NoSuchElementException:
            break

    # We assume that backend results corresponds to search page results
    data_backend = search_shop(browser, 'owasp')

    # Find two most expensive products from search results
    target_products = sorted(data_backend, key=lambda x: float(x['price']), reverse=True)[:2]

    # Add products to the basket
    for product in target_products:
        elem = browser.find_element_by_xpath('//*[contains(text(), "{}")]'.format(product['name']))
        wait_locator = (By.XPATH, '..//a[@ng-click="addToBasket(product.id)"]')
        WebDriverWait(elem, EXPLICIT_TIMEOUT).until(EC.element_to_be_clickable(wait_locator))
        elem.find_element_by_xpath('..//a[@ng-click="addToBasket(product.id)"]').click()

    # Go to the basket page
    sleep(1)            # TODO Change to webdriver explicit wait
    browser.find_element_by_xpath('//*[@href="#/basket"]').click()
    sleep(1)            # TODO Change to webdriver explicit wait

    # Find every item on basket page and create dict for each item
    WebDriverWait(browser, EXPLICIT_TIMEOUT).until(EC.presence_of_element_located((By.TAG_NAME, 'table')))
    table = browser.find_element_by_tag_name('table')
    data_rendered = []

    WebDriverWait(table, EXPLICIT_TIMEOUT).until(EC.presence_of_element_located((By.TAG_NAME, 'tr')))
    for tr in table.find_elements_by_xpath('.//tr'):
        WebDriverWait(table, EXPLICIT_TIMEOUT).until(EC.presence_of_element_located((By.TAG_NAME, 'td')))
        tds = tr.find_elements_by_xpath('.//td')
        if tds:
            td_data_rendered = {
                'name': tds[0].get_attribute('innerHTML'),
                'description': tds[1].get_attribute('innerHTML'),
                'price': tds[2].text,
                'quantity': tds[3].find_element_by_tag_name('span').text
            }
            data_rendered.append(td_data_rendered)

    # Check if correct items were added to basket
    assert [x['name'] for x in target_products] == [x['name'] for x in data_rendered]

    # Check if only one of each item was added to basket
    assert [x['quantity'] for x in data_rendered] == ['1', '1']

    # Check if price for each item is correct
    assert [x['price'] for x in target_products] == [x['price'] for x in data_rendered]
